package ccsecommovil.com.ccsecommovil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ccsecommovil.com.ccsecommovil.base.APIActivity;
import ccsecommovil.com.ccsecommovil.base.BACKRequestException;

public class Login extends APIActivity {
    String APILogin;
    EditText login_usuario;
    EditText login_password;
    Button login_iniciarsesion;
    int idUsuario=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        APILogin=   getString(R.string.API_Login);
        login_usuario=(EditText) findViewById(R.id.login_usuario);
        login_password=(EditText) findViewById(R.id.login_password);
        login_iniciarsesion=(Button) findViewById(R.id.login_iniciarsesion);
        idUsuario = this.obtenerVariableSharedPreferences("idUsuario",0);
        if(idUsuario>0) {
            IrListaEdificios(idUsuario);
        }else {
            login_iniciarsesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        verificarConexion();
                        Login.this.srRequest = new StringRequest(
                                Request.Method.POST,
                                Login.this.APILogin,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            if(!"".equals(response)) {
                                                JSONObject jo_respuesta = new JSONObject(response);
                                                idUsuario = jo_respuesta.getInt("id_usuario");
                                                Login.this.guardarVariableSharedPreferences("idUsuario", idUsuario);
                                                IrListaEdificios(idUsuario);
                                            }else{
                                                Toast.makeText(Login.this,"Usuario o contraseña incorrecto",Toast.LENGTH_LONG).show();
                                            }
                                        //} catch (JSONException e) {
                                        } catch (Exception e) {
                                            Toast.makeText(Login.this, "Error al leer la respuesta", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(Login.this, "Error al realizar la consulta.", Toast.LENGTH_LONG).show();
                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("email", login_usuario.getText().toString());
                                params.put("password", login_password.getText().toString());
                                return params;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(Login.this);
                        requestQueue.add(srRequest);
                    } catch (BACKRequestException e) {
                        Toast.makeText(Login.this,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
    protected void IrListaEdificios(int idUsuario){
        Intent iIrListaEdificios= new Intent(Login.this,ListaEdificios.class);
        iIrListaEdificios.putExtra("idUsuario",idUsuario);
        startActivity(iIrListaEdificios);
    }
}