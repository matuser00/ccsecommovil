package ccsecommovil.com.ccsecommovil.base;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.android.volley.request.StringRequest;

/**
 * Created by MAT17 on 05/04/2017.
 * Representa una actividad que usara un APIBackRequestJob para realizar llamados al API
 */

public abstract class APIActivity extends Activity {
    protected StringRequest srRequest;
    protected static SQLiteDatabase sqlitedb;
    protected static CCSECOMMovilDBHelper ccsecomMovilDBHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ccsecomMovilDBHelper= new CCSECOMMovilDBHelper(this);
        if(sqlitedb==null){
            sqlitedb=ccsecomMovilDBHelper.getWritableDatabase();
        }
    }
    //--------------- SHARED PREFERENCES ----------------------------------------
    String nombreSharedPreferences="spCCSECOMMovil";
    protected void guardarVariableSharedPreferences(String nombre,String valor){
        SharedPreferences prefs = getSharedPreferences(nombreSharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(nombre,valor);
        editor.commit();
    }
    protected void guardarVariableSharedPreferences(String nombre,int valor){
        SharedPreferences prefs = getSharedPreferences(nombreSharedPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(nombre,valor);
        editor.commit();
    }

    protected String obtenerVariableSharedPreferences(String nombre,String def){
        SharedPreferences prefs = getSharedPreferences(nombreSharedPreferences, Context.MODE_PRIVATE);
        return prefs.getString(nombre,def);
    }
    protected int obtenerVariableSharedPreferences(String nombre,int def){
        SharedPreferences prefs = getSharedPreferences(nombreSharedPreferences, Context.MODE_PRIVATE);
        return prefs.getInt(nombre,def);
    }
    protected void verificarConexion() throws BACKRequestException {
        NetworkInfo i = ((ConnectivityManager) this.getApplicationContext().getSystemService(this.getBaseContext().CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (i == null || !i.isConnected() || !i.isAvailable())
            throw new BACKRequestException("No se pudo consultar la información, verifica tu conexión de red");
    }
}
