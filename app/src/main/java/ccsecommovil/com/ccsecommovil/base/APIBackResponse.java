package ccsecommovil.com.ccsecommovil.base;

import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by MAT17 on 05/04/2017.
 * Clase que contiene funciona como respuesta de un request mediante APIBackRequestJob
 */

public class APIBackResponse {
    public String serverMessage;//Mensaje del servidor
    public int serverCode;//Codigo del servidor
    public String message;//Mensaje o string de la estructura devuelta por el servidor
    public String url;//url que se consulto
    //---- Tipos de respuestas JSON, depende del tipo que devuelva la url consultada ------
    public JSONArray jsonArrayResponse;//Respuesta en forma json array
    public JSONObject jsonObjectResponse;//Respuesta en forma de json object
    public Bitmap bitmapImage;//Respuesta para cuando se devuelve una imagen
}
