package ccsecommovil.com.ccsecommovil.base;

import java.util.EventListener;

public interface BACKRequestListener extends EventListener {
    public void onExito(BACKRequestEvent e);
    public void onError(BACKRequestEvent e);
}
