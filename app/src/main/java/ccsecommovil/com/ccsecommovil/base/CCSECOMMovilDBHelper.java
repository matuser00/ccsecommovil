package ccsecommovil.com.ccsecommovil.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by MAT17 on 04/06/2017.
 */

public class CCSECOMMovilDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="ccsecommovil.db";

    //Código de creacion de tablas para edificios y departamentos:
    public static final String SQL_CREATE_TABLE_EDIFICIOS="CREATE TABLE "+CCSECOMMovilReaderContract.EdificiosAvanceEntry.TABLE_NAME+
            "("
            +CCSECOMMovilReaderContract.EdificiosAvanceEntry.COLUMN_NAME_IDEDIFICIO+" INT,"
            +CCSECOMMovilReaderContract.EdificiosAvanceEntry.COLUMN_NAME_NUMERO_DEPARTAMENTOSREGISTRADOS+ " INT,"
            +CCSECOMMovilReaderContract.EdificiosAvanceEntry.COLUMN_NAME_TOTALDEPARTAMENTOS+ " INT "
            +")";
    public static final String SQL_CREATE_TABLE_DEPARTAMENTOS="CREATE TABLE "+CCSECOMMovilReaderContract.DepartamentosRegistradosEntry.TABLE_NAME+
            "("
            +CCSECOMMovilReaderContract.DepartamentosRegistradosEntry.COLUMN_NAME_IDDEPARTAMENTO+" INT,"
            +CCSECOMMovilReaderContract.DepartamentosRegistradosEntry.COLUMN_NAME_FECHAREGISTRO+ " DATETIME"
            +")";
    //Código para baja de tablas para edificios y departamentos:
    public static final String SQL_DELETE_TABLE_EDIFICIOS="DROP TABLE IF EXISTS "+CCSECOMMovilReaderContract.EdificiosAvanceEntry.TABLE_NAME;
    public static final String SQL_DELETE_TABLE_DEPARTAMENTOS="DROP TABLE IF EXISTS "+CCSECOMMovilReaderContract.DepartamentosRegistradosEntry.TABLE_NAME;

    public CCSECOMMovilDBHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_EDIFICIOS);
        db.execSQL(SQL_CREATE_TABLE_DEPARTAMENTOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE_EDIFICIOS);
        db.execSQL(SQL_DELETE_TABLE_DEPARTAMENTOS);
        onCreate(db);
    }
    @Override
    public void onDowngrade(SQLiteDatabase db,int oldVersion, int newVersion){
        onUpgrade(db,oldVersion,newVersion);
    }
}