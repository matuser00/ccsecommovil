package ccsecommovil.com.ccsecommovil.base;

import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONObject;

public class BACKRequestEvent {
    String direccion;
    JSONObject respuestaJSON;
    JSONArray respuestaJSONArray;
    int exitoError = 0;
    String mensaje = "";
    Bitmap bitmapImagen = null;
}
