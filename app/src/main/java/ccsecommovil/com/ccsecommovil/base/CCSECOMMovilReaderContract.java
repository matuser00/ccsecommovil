package ccsecommovil.com.ccsecommovil.base;

import android.provider.BaseColumns;

/**
 * Created by MAT17 on 04/06/2017.
 */

public final class CCSECOMMovilReaderContract {
    private CCSECOMMovilReaderContract(){}
    public static class EdificiosAvanceEntry{
        public static final String TABLE_NAME="EdificiosAvance";
        public static final String COLUMN_NAME_IDEDIFICIO="idEdificio";
        public static final String COLUMN_NAME_NUMERO_DEPARTAMENTOSREGISTRADOS="numeroDepartamentosRegistrados";
        public static final String COLUMN_NAME_TOTALDEPARTAMENTOS="totalDepartamentos";
    }
    public static class DepartamentosRegistradosEntry implements BaseColumns {
        public static final String TABLE_NAME="DepartamentosRegistrados";
        public static final String  COLUMN_NAME_IDEDIFICIO="idEdificio";
        public static final String COLUMN_NAME_IDDEPARTAMENTO="idDepartamento";
        public static final String COLUMN_NAME_FECHAREGISTRO="fechaRegistro";
    }
}
