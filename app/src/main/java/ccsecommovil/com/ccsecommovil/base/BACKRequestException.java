package ccsecommovil.com.ccsecommovil.base;

/**
 * Excepcion de un request al back
 */
public class BACKRequestException extends Exception {
    public BACKRequestException(String mensaje){
        super(mensaje);
    }
}