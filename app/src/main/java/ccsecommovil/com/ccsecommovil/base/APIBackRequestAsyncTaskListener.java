package ccsecommovil.com.ccsecommovil.base;

/**
 * Created by MAT17 on 05/04/2017.
 */

public interface APIBackRequestAsyncTaskListener {
    void onSuccess(APIBackResponse respuesta);//Cuando ubo éxito en procesar el request
    void onError(APIBackResponse respuesta);//Cuando ubo un error al procesar el request
}
