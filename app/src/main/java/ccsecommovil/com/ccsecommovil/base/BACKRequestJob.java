package ccsecommovil.com.ccsecommovil.base;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.EventListener;
import java.util.List;

/**
 * Created by Tomás on 14/10/16.
 * Clase que contiene el funcionamiento básico para realizar request con el back de la aplicación.
 */
public class BACKRequestJob extends AsyncTask<String, Void, String> {

    List<BACKRequestListener> listaEscuchasBACKRequest = new ArrayList<BACKRequestListener>();
    private String direccion;
    private Activity actividad;
    private JSONObject respuestaJSON;
    private JSONArray respuestaJSONArray;
    private Bitmap bitmapImagen;
    public Uri archivoASubir = null;
    public String idUsuario="0";
    public String[] parametrosPOST=null;
    public static final int OPERACION_SUBIRARCHIVO = 1;
    public static final int OPERACION_OBTENERDATOS = 0;
    public static final int OPERACION_DESCARGARARCHIVO = 2;
    public int operacion = OPERACION_OBTENERDATOS;
    public static String METODO_GET="GET";
    public static String METODO_POST="POST";
    public String metodo=METODO_GET;
    BACKRequestEvent evento = new BACKRequestEvent();

    public BACKRequestJob(Activity iactividad, String idireccion) {
        actividad = iactividad;
        direccion = idireccion;
    }

    //Verifica la conexión a internet. En caso de no tener acceso dispara un excepcion.
    private void verificarConexionInternet(Activity actividad) throws BACKRequestException {
        NetworkInfo i = ((ConnectivityManager) actividad.getApplicationContext().getSystemService(actividad.getBaseContext().CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (i == null || !i.isConnected() || !i.isAvailable())
            throw new BACKRequestException("No se pudo consultar la información, verifica tu conexión de red");
    }

    //Agrega escuchas de back request.
    public void addBACKRequestListener(BACKRequestListener escucha) {
        listaEscuchasBACKRequest.add(escucha);
    }

    //Ejecucion de request JSON.
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void ejecutarRequest(final Activity actividad, String direccion) throws BACKRequestException {
        verificarConexionInternet(actividad);
        if (operacion == OPERACION_SUBIRARCHIVO) {
            HttpURLConnection conn = null;

            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;

            HttpURLConnection connection = null;
            DataOutputStream outputStream = null;
            DataInputStream inputStream = null;


            try {
                FileInputStream fileInputStream=null;
                if(archivoASubir!=null) {
                    fileInputStream= new FileInputStream(new File(getPath(actividad,archivoASubir)) );
                }


                URL url = new URL(direccion);
                connection = (HttpURLConnection) url.openConnection();

                // Allow Inputs &amp; Outputs.
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);

                // Set HTTP method to POST.
                connection.setRequestMethod(metodo);

                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("Cache-Control", "no-cache");

                //if(archivoASubir!=null) {
                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                //}
                outputStream = new DataOutputStream( connection.getOutputStream() );

                String lectura=parametrosPOST[0];
                String id_depto=parametrosPOST[1];
                /*String fechaConsumo1=parametrosPOST[2];
                String fechaConsumo2=parametrosPOST[2];
                String fechaTomaLectura=(new Date()).toString();*/



                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                //outputStream.writeBytes("Content-Disposition: form-data; name=\"mensaje\"" + mensajeFoto!=""?(lineEnd+mensajeFoto+lineEnd):"");
                //outputStream.writeBytes("Content-Disposition: form-data; name=\"mensaje\"" + lineEnd+lineEnd+mensajeFoto+lineEnd);
                //mensajeFoto= Charset.forName("UTF-8").encode(mensajeFoto);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"lectura\""+lineEnd+lineEnd+lectura+lineEnd);
                outputStream.writeBytes(lineEnd);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"id_depto\"" +id_depto);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                if(archivoASubir!=null) {
                    outputStream.writeBytes("Content-Disposition: form-data; name=\"foto_lectura\";filename=\"" + getPath(actividad, archivoASubir) + "\"" + lineEnd);
                }
                outputStream.writeBytes(lineEnd);
                if(archivoASubir!=null) {
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {

                        outputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    }
                }

                // send multipart form data necesssary after file data...
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                outputStream.writeBytes(lineEnd);

                if(archivoASubir!=null) {
                    fileInputStream.close();
                }
                outputStream.flush();
                outputStream.close();

                if (200 <= connection.getResponseCode() && connection.getResponseCode() <= 299) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
                    String line, newjson = "";
                    while ((line = reader.readLine()) != null) {
                        newjson += line;
                    }
                    evento.mensaje=newjson;
                    String json = newjson.toString();

                    reader.close();
                    try {
                        if(json!="") {
                            respuestaJSON = new JSONObject(json);
                            evento.exitoError = 1;
                        }else {
                            evento.exitoError=1;
                        }
                    } catch (JSONException je) {
                        try {
                            if(json!="[]") {
                                respuestaJSONArray = new JSONArray(json);
                                evento.exitoError = 1;
                            }else {
                                evento.exitoError = 1;
                            }
                        } catch (JSONException e) {
                            try {
                                InputStream in = new URL(direccion).openStream();
                                bitmapImagen = BitmapFactory.decodeStream(in);
                            } catch (Exception ex) {
                                e.printStackTrace();
                                throw new BACKRequestException("Error: "+ex.getMessage());
                            }
                        }
                    }
                }else {
                    throw new BACKRequestException("Error al consultar el link");
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new BACKRequestException(e.getMessage());
            }
        }
        HttpURLConnection conn = null;

        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        if (operacion == OPERACION_OBTENERDATOS) {

            try {
                StringBuilder parametrosCodificados=new StringBuilder();
                boolean primero=true;
                if(parametrosPOST.length>0) {
                    for (String parametro : parametrosPOST) {
                        int lenparametro=parametro.trim().length();
                        if(lenparametro>0) {
                            String[] propiedadValor = parametro.split("=");
                            if (primero)
                                primero = false;
                            else
                                parametrosCodificados.append("&");
                            parametrosCodificados.append(URLEncoder.encode(propiedadValor[0], "UTF-8"));
                            parametrosCodificados.append("=");
                            parametrosCodificados.append(URLEncoder.encode(propiedadValor[1], "UTF-8"));
                        }
                    }
                }

                String urlParameters  = parametrosCodificados.toString();
                byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                int    postDataLength = postData.length;
                String request        = direccion;
                URL    url            = new URL( request );
                conn= (HttpURLConnection) url.openConnection();
                if(metodo==METODO_POST) {
                    conn.setDoOutput(true);
                    conn.setDoInput(true);
                    conn.setInstanceFollowRedirects(false);
                    conn.setRequestMethod(metodo);
                }
                if(metodo==METODO_POST)
                    conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                if(parametrosPOST.length>0) {
                    if(parametrosPOST[0]!="")
                        conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
                }
                conn.setUseCaches( false );

                if(postDataLength>0) {
                    try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                        wr.write(postData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
                    String line, newjson = "";
                    while ((line = reader.readLine()) != null) {
                        newjson += line;
                    }
                    evento.mensaje=newjson;
                    String json = newjson.toString();
                    reader.close();
                    try {
                        if(json!="") {
                            respuestaJSON = new JSONObject(json);
                            evento.exitoError = 1;
                        }else {
                            evento.exitoError=1;
                        }
                    } catch (JSONException je) {
                        try {
                            if(json!="[]") {
                                respuestaJSONArray = new JSONArray(json);
                                evento.exitoError = 1;
                            }else {
                                evento.exitoError = 1;
                            }
                        } catch (JSONException e) {
                            try {
                                InputStream in = new URL(direccion).openStream();
                                bitmapImagen = BitmapFactory.decodeStream(in);
                            } catch (Exception ex) {
                                e.printStackTrace();
                                throw new BACKRequestException("Error: "+ex.getMessage());
                            }
                        }
                    }
                }else {
                    throw new BACKRequestException("Error al consultar el link");
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
                throw new BACKRequestException(e.getMessage());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                throw new BACKRequestException(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
                throw new BACKRequestException(e.getMessage());
            }
        }
    }

    private boolean exito = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected String doInBackground(String... params) {
        try {
            if(params.length>0)
                parametrosPOST=params;
            ejecutarRequest(actividad, direccion);
            evento.exitoError = 1;
        } catch (Exception ex) {
            evento.exitoError = 0;
            evento.mensaje = ex.getMessage();
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context
     *            The context.
     * @param uri
     *            The Uri to query.
     * @param selection
     *            (Optional) Filter used in the query.
     * @param selectionArgs
     *            (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }
    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri
                .getAuthority());
    }
    @Override
    protected void onPostExecute(String result) {
        if (evento.exitoError == 1) {
            for (BACKRequestListener escucha : listaEscuchasBACKRequest) {
                evento.direccion = direccion;
                evento.respuestaJSON = respuestaJSON;
                evento.respuestaJSONArray = respuestaJSONArray;
                evento.bitmapImagen = bitmapImagen;
                escucha.onExito(evento);
            }
        } else {
            evento.direccion = direccion;
            evento.respuestaJSON = null;
            for (BACKRequestListener escucha : listaEscuchasBACKRequest) {
                escucha.onError(evento);
            }
        }
    }

    /*--- PARA SUBIR ARCHIVOS ---*/
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}

