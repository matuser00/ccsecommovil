package ccsecommovil.com.ccsecommovil.base;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by MAT17 on 05/04/2017.
 * Clase que se encarga de hacer una tarea asincrona de request para el api del back
 */

public class APIBackRequestJob extends AsyncTask<String, Void, String> {
    protected Activity actividad;
    protected APIBackRequestAsyncTaskListener escucha;

    protected ArrayList<APIParametro> parametros= new ArrayList<APIParametro>();
    public APIBackRequestJob(Activity iActividad) {
        actividad=iActividad;
        escucha=(APIBackRequestAsyncTaskListener)iActividad;
    }

    //------ FUNCIONES BÀSICAS -----------------------------------------------------------------------------
    //Set de escucha de exito o error de la tarea
    public void setAPIBackRequestAsyncTaskListener(APIBackRequestAsyncTaskListener iEscucha){
        escucha=iEscucha;
    }
    //Verifica la conexión a internet. En caso de no tener acceso dispara un excepcion.
    private void verificarConexionInternet(Activity actividad) throws BACKRequestException {
        NetworkInfo i = ((ConnectivityManager) actividad.getApplicationContext().getSystemService(actividad.getBaseContext().CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (i == null || !i.isConnected() || !i.isAvailable())
            throw new BACKRequestException("No se pudo consultar la información, verifica tu conexión de red");
    }

    HttpURLConnection connection = null;
    DataOutputStream outputStream = null;
    OutputStreamWriter outputStreamWriter=null;
    DataInputStream inputStream = null;
    URL url = null;

    String lineEnd = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";
    //Funcion que abre la conexion de un link del API indicado con el mètodo indicado
    String link;
    String metodo;
    public HttpURLConnection abrirConexionAPILink(String link,String metodo) throws BACKRequestException {
        verificarConexionInternet(actividad);
        try {
            this.link=link;
            this.metodo=metodo;
            url = new URL(link);
            connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs &amp; Outputs.
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            // Set HTTP method to POST.
            connection.setRequestMethod(metodo);

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Cache-Control", "no-cache");

            if(metodo=="POST")
                connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");

            outputStream = new DataOutputStream( connection.getOutputStream() );

            //Agregar parametros:
            //(.*/)*.+\.(png|jpg|gif|bmp|jpeg|PNG|JPG|GIF|BMP)$
            for (APIParametro parametro:parametros) {
                String patron="(.*/)*.+\\.(png|jpg|gif|bmp|jpeg|PNG|JPG|GIF|BMP)$";
                if(parametro.valor.matches(patron)) {
                    agregarParametroArchivo(parametro.nombre,parametro.valor);
                }else {
                    agregarParametroTexto(parametro.nombre,parametro.valor,parametro.contentDisposition,parametro.utf8);
                }
            }
        } catch (MalformedURLException e) {
            new BACKRequestException("URL incorrecta.");
        } catch (IOException e) {
            new BACKRequestException("Problema al abrir la conexión.");
        }
        return connection;
    }

    //Agregar parametros ------------------------------------------------------------------------------------------
    public void agregarParametro(String nombre,String valor,String contendDisposition, Boolean escribirUTF) {
        APIParametro parametro= new APIParametro();
        parametro.nombre=nombre;
        parametro.valor=valor;
        parametro.contentDisposition=contendDisposition;
        parametro.utf8=escribirUTF;
        parametros.add(parametro);
    }
    //Funcion que agrega parametros en modo de texto al request:
    public static final String CONTENT_DISPOSITION_FORMDATA="Content-Disposition: form-data;";
    String parametrosGET="";
    private void agregarParametroTexto(String nombre,String valor,String contentDisposition,Boolean escribirUTF) throws IOException {
        outputStream.writeBytes(contentDisposition+" name=\""+nombre+"\""+lineEnd+lineEnd);
        if(connection.getRequestMethod()=="POST") {
            if (escribirUTF)
                outputStream.writeUTF(valor);
            else
                outputStream.writeBytes(valor);
            outputStream.writeBytes(lineEnd);
        }
        if(connection.getRequestMethod()=="GET") {
            if(parametrosGET==""){
                parametrosGET="?"+nombre+"="+ URLEncoder.encode(valor,"UTF-8");
            }else{
                parametrosGET="&"+nombre+"="+ URLEncoder.encode(valor,"UTF-8");
            }
        }
    }
    //Funcion que agrega parametros en modo de archivo al request:
    private void agregarParametroArchivo(String nombre,String direccionArchivo) throws IOException {
        connection.setRequestProperty("ENCTYPE", "multipart/form-data");
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
        Uri uriArchivo=Uri.parse(direccionArchivo);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + getPath(actividad, uriArchivo) + "\"" + lineEnd);
        FileInputStream fileInputStream= new FileInputStream(new File(getPath(actividad,uriArchivo)));
        //Escribir archivo
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];
        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        outputStream.writeBytes(lineEnd);
        outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        outputStream.writeBytes(lineEnd);
        fileInputStream.close();
    }

    APIBackResponse respuestaRequest=null;
    //Funcion que realiza el request:
    private void realizarRequest(){
        try {
            outputStream.flush();
            outputStream.close();
            //Procesar respuesta:
            respuestaRequest=new APIBackResponse();
            respuestaRequest.serverCode=connection.getResponseCode();
            respuestaRequest.serverMessage=connection.getResponseMessage();
            respuestaRequest.url=url.toString();
            if (200 <= connection.getResponseCode() && connection.getResponseCode() <= 299) {
                respuestaRequest=new APIBackResponse();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));
                String line,json = "";
                while ((line = reader.readLine()) != null) {
                    json += line;
                }
                respuestaRequest.message=json;
                reader.close();
                //Obtener respuestas como JSON o JSON Array:
                try {
                    if(json!="") {
                        respuestaRequest.jsonObjectResponse = new JSONObject(json);
                    }
                } catch (JSONException je) {
                    try {
                        if(json!="[]") {
                            respuestaRequest.jsonArrayResponse = new JSONArray(json);
                        }
                    } catch (JSONException e) {
                        ;
                    }
                }
                escucha.onSuccess(respuestaRequest);
            }else{
                escucha.onError(respuestaRequest);
            }
        } catch (IOException e) {
            respuestaRequest.message="Error al obtener la respuesta";
            escucha.onError(respuestaRequest);
        }
    }

    //------ FUNCIONES PARA OBTENER RUTA DEL ARCHIVO -------------------------------------------------------
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context
     *            The context.
     * @param uri
     *            The Uri to query.
     * @param selection
     *            (Optional) Filter used in the query.
     * @param selectionArgs
     *            (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }
    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri
     *            The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri
                .getAuthority());
    }
    //------ FUNCIONES PARA EJECUTAR LA TAREA --------------------------------------------------------------
    @Override
    protected String doInBackground(String... params) {
        try {
            abrirConexionAPILink(params[0],params[1]);
            realizarRequest();
        } catch (Exception e) {
            APIBackResponse respuesta= new APIBackResponse();
            respuesta.message=e.getMessage();
            escucha.onError(respuesta);
        }
        return null;
    }
}