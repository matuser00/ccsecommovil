package ccsecommovil.com.ccsecommovil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ccsecommovil.com.ccsecommovil.base.APIActivity;
import ccsecommovil.com.ccsecommovil.base.BACKRequestException;

public class ListaEdificios extends APIActivity {
    String APIListaEdificios;
    ArrayList<Edificio> edificios;
    protected int idUsuario=0;
    ListView lista;
    Button reintentarconectarse_boton;
    TextView reintentarconectarce_texto;
    View viewReintentarconectarce;
    LinearLayout listaedificios_contenido;
    ProgressDialog pdProgreso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_edificios);

        edificios=new ArrayList<Edificio>();
        idUsuario= this.getIntent().getIntExtra("idUsuario",0);
        if(idUsuario==0){
            Intent iLogin= new Intent(this,Login.class);
            startActivity(iLogin);
        }else {
            APIListaEdificios = getString(R.string.API_Edificios);
            viewReintentarconectarce=findViewById(R.id.listaedificios_includereintentarconectarse);
            listaedificios_contenido=(LinearLayout)findViewById(R.id.listaedificios_contenido);
            reintentarconectarse_boton=(Button) viewReintentarconectarce.findViewById(R.id.reintentarconectarse_boton);

            reintentarconectarse_boton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    realizarRequest();
                }
            });

            reintentarconectarce_texto=(TextView)viewReintentarconectarce.findViewById(R.id.reintentarconectarse_texto);

            lista= (ListView)findViewById(R.id.listaedificios_lista);
            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Edificio edificio=ListaEdificios.this.edificios.get(position);
                    Intent iListaDepartamentos= new Intent(ListaEdificios.this,ListaDepartamentos.class);
                    iListaDepartamentos.putExtra("nombreEdificio",edificio.nombre);
                    iListaDepartamentos.putExtra("idEdificio",edificio.idEdificio);
                    startActivity(iListaDepartamentos);
                }
            });
            realizarRequest();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        realizarRequest();
    }
    JSONArray lista_deptos=null;
    public void realizarRequest(){
        pdProgreso=ProgressDialog.show(this,"Obteniendo datos.","Espere un momento...",true,true);
        try {
            verificarConexion();
            srRequest = new StringRequest(
                    Request.Method.GET,
                    ListaEdificios.this.APIListaEdificios+"/"+idUsuario,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject joRespuesta = new JSONObject(response);
                                joRespuesta=joRespuesta.getJSONObject("root");
                                JSONObject joOperacion = joRespuesta.getJSONObject("operacion");
                                if (joOperacion.getString("codigo") == "2") {
                                    Toast.makeText(ListaEdificios.this, joOperacion.getString("mensaje"), Toast.LENGTH_LONG).show();
                                } else {
                                    ListaEdificios.this.cargarEdificios(joRespuesta.getJSONObject("respuesta").getJSONArray("edificios"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            viewReintentarconectarce.setVisibility(View.GONE);
                            listaedificios_contenido.setVisibility(View.VISIBLE);
                            try {
                                pdProgreso.dismiss();
                            }catch(Exception ex){;}
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(com.android.volley.error.VolleyError error) {
                    reintentarconectarce_texto.setText("Error al cargar la respuesta.");
                    viewReintentarconectarce.setVisibility(View.VISIBLE);
                    pdProgreso.cancel();
                    listaedificios_contenido.setVisibility(View.GONE);
                }
            }
            );

                RequestQueue requestQueue = Volley.newRequestQueue(ListaEdificios.this);
                requestQueue.getCache().clear();
                requestQueue.add(srRequest);

        } catch (Exception e) {
            pdProgreso.cancel();
            reintentarconectarce_texto.setVisibility(View.VISIBLE);
            reintentarconectarce_texto.setText("Problema con la conexión, por favor verifícala.");
        }
    }



    public void cargarEdificios(JSONArray arregloEdificios){
        //Obtener edificios que ya fueron registrados
        /*DAOEdificio dao= new DAOEdificio(this,sqlitedb);
        List<Edificio> listaEdificiosRegistrados=dao.obtenerEdificiosRegistrosCompletos();*/
        try{//Limpiar elementos anteriores de la lista:
            ((EdificiosAdapter)lista.getAdapter()).clear();
        }catch(Exception ex){;}
        for (int i=0;i<arregloEdificios.length();i++) {
            try {
                JSONObject joEdificio=arregloEdificios.getJSONObject(i);
                Edificio edificio= new Edificio(joEdificio.getInt("id_edificio"),joEdificio.getJSONObject("edificio").getString("edificio"));
                //Agregamos los edificios que no estén registrados:
                /*if(!listaEdificiosRegistrados.contains(edificio))
                    edificios.add(edificio);*/
                String departamentosRegistradosEnEdificio=obtenerVariableSharedPreferences("deptos_registrados_"+edificio.idEdificio,"");
                edificio.totalDepartamentos=joEdificio.getInt("total_deptos");
                lista_deptos=joEdificio.getJSONArray("deptos");
                edificio.departamentosRegistrados=lista_deptos.length();
                edificios.add(edificio);
            } catch (JSONException e) {
                Toast.makeText(this,"Problema al cargar la información.",Toast.LENGTH_LONG).show();
                break;
            }
        }
        //Mostrar lista:
        //Cargar datos obtenidos a la lista:
        EdificiosAdapter aEdificios= new EdificiosAdapter(ListaEdificios.this,edificios);
        Parcelable state =lista.onSaveInstanceState();
        lista.setAdapter(aEdificios);
        lista.onRestoreInstanceState(state);
        getIndexList();
        displayIndex();
    }
    LinkedHashMap<String,Integer> mapIndex;
    private void getIndexList(){
        mapIndex=new LinkedHashMap<String,Integer>();
        mapIndex.clear();
        int i=0;
        for (Edificio edificio: edificios) {
            String index=edificio.nombre.substring(0,1);
            if(mapIndex.get(index)==null)
                mapIndex.put(index,i);
            i++;
        }
    }

    private void displayIndex(){
        LinearLayout indexLayout=(LinearLayout)findViewById(R.id.listaedificios_indice);
        TextView textView;
        List<String> indexList=new ArrayList<String>(mapIndex.keySet());
        indexLayout.removeAllViews();
        for (String index:indexList
             ) {
            textView=(TextView)getLayoutInflater().inflate(R.layout.itemalfabeto,null);
            textView.setText(index);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView selectedIndex=(TextView)v;
                    lista.setSelection(mapIndex.get(selectedIndex.getText()));
                }
            });
            indexLayout.addView(textView);
        }
    }
}
class EdificiosAdapter extends ArrayAdapter<Edificio> {
    private ArrayList<Edificio> listaEdificios=null;
    private final Activity contexto;

    public EdificiosAdapter(Activity context, ArrayList<Edificio> listaEdificios) {
        super(context, 0,listaEdificios);
        this.contexto= context;
        this.listaEdificios=listaEdificios;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position

        ViewHolder holder=null;
        Edificio edificio= getItem(position);
        //Se maneja cache de vista:
        holder= new ViewHolder();
        LayoutInflater inflator = contexto.getLayoutInflater();

        convertView = inflator.inflate(R.layout.edificiolista, null);
        holder.mensaje=(TextView) convertView.findViewById(R.id.edificiolista_nombre);
        holder.imagenMensaje=(ImageView) convertView.findViewById(R.id.edificiolista_indicador);

        convertView.setTag(holder);
        convertView.setTag(R.id.edificiolista_nombre, holder.mensaje);
        convertView.setTag(R.id.edificiolista_indicador,holder.imagenMensaje);
        if(edificio.departamentosRegistrados==edificio.totalDepartamentos&&edificio.departamentosRegistrados>0)
            holder.mensaje.setBackgroundColor(Color.LTGRAY);
        holder.mensaje.setText(edificio.nombre);
        //holder.imagenMensaje.setVisibility((edificio.getRegistroCompleto()?View.INVISIBLE:View.VISIBLE));
        return convertView;
    }

    @Override
    public int getCount() {
        return listaEdificios.size();
    }

    @Override
    public Edificio getItem(int position) {
        return listaEdificios.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    static class ViewHolder {
        protected String tipo;//1:Enviado por usuario,2:SECOM
        protected TextView mensaje;
        protected ImageView imagenMensaje;
    }
}
