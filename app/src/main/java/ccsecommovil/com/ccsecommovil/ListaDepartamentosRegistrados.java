package ccsecommovil.com.ccsecommovil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ccsecommovil.com.ccsecommovil.base.APIActivity;

public class ListaDepartamentosRegistrados extends APIActivity {
    ListView lvListaExtemporaneos;
    String APIListaExtemporaneos;
    int idUsuario=-1;
    int idTorre=-1;
    ProgressDialog pdProgreso;
    ArrayAdapter<Departamento> departamentoArrayAdapter;
    public ImageView ivLogo;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_registrados);
        ivLogo=(ImageView)findViewById(R.id.registrados_logo);
        lvListaExtemporaneos= (ListView)findViewById(R.id.registrados_lista);
        APIListaExtemporaneos=getString(R.string.API_Registrados);
        idUsuario=obtenerVariableSharedPreferences("idUsuario", -1);
        idTorre=getIntent().getIntExtra("idTorre",-1);
        if(idTorre>-1&&idUsuario>-1) {
            pdProgreso= ProgressDialog.show(this,"Cargando registrados.","Por favor espere.",true,false);
            this.srRequest = new StringRequest(
                    Request.Method.GET,
                    this.APIListaExtemporaneos+"/"+idTorre,
                    //this.APIDepartamentos,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                lvListaExtemporaneos.setAdapter(null);
                                JSONObject jsonRespuesta= new JSONObject(response);
                                jsonRespuesta=jsonRespuesta.getJSONObject("root");
                                JSONObject joOperacion=jsonRespuesta.getJSONObject("operacion");
                                if(joOperacion.getInt("codigo")==1){
                                    //Llenar lista de departamentos:
                                    JSONArray jaDepartamentos= jsonRespuesta.getJSONArray("respuesta");
                                    if(jaDepartamentos.length()>0) {
                                        for (int i = 0; i < jaDepartamentos.length(); i++) {
                                            JSONObject jsonDepartamento = jaDepartamentos.getJSONObject(i);
                                            Departamento departamento = new Departamento();
                                            departamento.verificado = false;
                                            departamento.idDepartamento = jsonDepartamento.getInt("id_depto");
                                            departamento.nombre = jsonDepartamento.getString("depto");
                                            //Agregamos los departamentos que no estén regitrados:
                                    /*if(!departamentosRegistrados.contains(departamento))
                                        ;*/
                                            departamentoArrayAdapter.add(departamento);
                                        }
                                        lvListaExtemporaneos.setVisibility(View.VISIBLE);
                                        lvListaExtemporaneos.setAdapter(departamentoArrayAdapter);
                                    }else{
                                        ivLogo.setVisibility(View.VISIBLE);
                                        Toast.makeText(ListaDepartamentosRegistrados.this,"No hay ningún departamento registrado por el momento.",Toast.LENGTH_LONG).show();
                                    }
                                    pdProgreso.dismiss();
                                }else{
                                    ivLogo.setVisibility(View.VISIBLE);
                                    Toast.makeText(ListaDepartamentosRegistrados.this,"No hay ningún departamento registrado por el momento.",Toast.LENGTH_LONG).show();
                                    pdProgreso.cancel();
                                }
                            } catch (JSONException e) {
                                ivLogo.setVisibility(View.VISIBLE);
                                Toast.makeText(ListaDepartamentosRegistrados.this,"Error al mostrar los departamentos.",Toast.LENGTH_LONG).show();
                                pdProgreso.cancel();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    ivLogo.setVisibility(View.VISIBLE);
                    Toast.makeText(ListaDepartamentosRegistrados.this,"Error al cargar los departamentos.",Toast.LENGTH_LONG).show();
                    pdProgreso.cancel();
                }
            }
            )/* {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUsuario", "" + ListaDepartamentos.this.idUsuario);
                params.put("idEdificio", "" + ListaDepartamentos.this.idEdificio);
                return params;
            }
        }*/;
            RequestQueue requestQueue = Volley.newRequestQueue(ListaDepartamentosRegistrados.this);
            requestQueue.add(srRequest);
        }else {
            Intent iListaEdificios = new Intent(this, ListaEdificios.class);
            startActivity(iListaEdificios);
        }
    }
}
